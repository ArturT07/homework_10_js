const tabs = document.querySelectorAll('.tabs-title');
const contentItems = document.querySelectorAll('.tabs-content li');

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        // Remove 'active' class from all tabs and content items
        tabs.forEach(t => t.classList.remove('active'));
        contentItems.forEach(item => item.style.display = 'none');

        // Add 'active' class to the clicked tab and show corresponding content
        tab.classList.add('active');
        const tabId = tab.getAttribute('data-tab');
        const contentToShow = document.getElementById(tabId);
        contentToShow.style.display = 'block';
    });
});

// Show initial content
contentItems[0].style.display = 'block';